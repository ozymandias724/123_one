<?php 
	if ( !Pagedata::is_active_page('menu') ) {
		header( "Location: " . site_url() . "/404.php" );
	}
	get_header();

	get_template_part('modules/menu');
	get_template_part('partials/global', 'recent_posts');
	get_template_part('partials/global', 'contact');

	get_footer();
 ?>