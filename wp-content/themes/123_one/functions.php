<?php 

	function enqueue_theme_scripts() {

		$parent_style = 'parent'; // parent theme enqueue tag in parent theme functions.php

		$parent_script = 'parent-main';

		wp_enqueue_style( $parent_style, get_template_directory_uri() . '/build/css/build.css' );
		
		wp_enqueue_style( 'child',
		    get_stylesheet_directory_uri() . '/build/css/build.css',
		    array( $parent_style ),
		    wp_get_theme()->get('Version')
		);

		wp_dequeue_script( 'parent-exec' );

		wp_enqueue_script( 'child-main',
		    get_stylesheet_directory_uri() . '/build/js/build.js',
		    array( $parent_script ),
		    wp_get_theme()->get('Version')
		);

		
		wp_enqueue_script( 'child-exec',
		    get_stylesheet_directory_uri() . '/build/js/exec.js',
		    array( $parent_script ),
		    wp_get_theme()->get('Version')
		);
	}
	add_action( 'wp_enqueue_scripts', 'enqueue_theme_scripts', 100 );



	add_filter('do_remove_homepage_anchors', 'remove_homepage_anchors');
	function remove_homepage_anchors($page_url){
		return str_replace('#', '', $page_url);
	}

	function wpse_233129_admin_menu_items() {
	    global $menu;
	    $menu[5][0] = 'Blog Posts';
	    foreach ( $menu as $key => $value ) {
	        if ( 'edit.php' == $value[2] ) {
	            $oldkey = $key;
	        }
	    }
	    // change Posts menu position in the backend
	    $newkey = 83; // use whatever index gets you the position you want
	    // if this key is in use you will write over a menu item!
	    $menu[$newkey]=$menu[$oldkey];
	    unset($menu[$oldkey]);
	}


	function custom_color_css(){
		if( get_field('buttons-underlines-toggle', 'option') ):
			$color = get_field('buttons-underlines', 'option');
		?>
			<style type="text/css">
				.services-services-grid-item-header{
					background-color: <?php echo $color; ?>;
				}
			</style>
		<?php
		endif;
	}
	add_action( 'wp_head', 'custom_color_css' );
	
?>